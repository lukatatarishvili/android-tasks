interface War {
    fun Destroy()
}

class RussiaDown : War {
    override fun Destroy() {
        println("Destroy Russia")
    }
}

class BelarusDown : War {
    override fun Destroy() {
        println("Destroy Belarus")
    }
}

class WarFactory {
    fun create(type: String): War? {
        return when (type) {
            "RussiaDown" -> RussiaDown()
            "BelarusDown" -> BelarusDown()
            else -> null
        }
    }
}