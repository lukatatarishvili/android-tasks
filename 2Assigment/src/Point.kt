
class Point(private val x: Int = 0, private val y: Int = 0) {

    override fun toString(): String {
        return "(x = $x, y = $y)"
    }

    override fun equals(other: Any?): Boolean {

        return other is Point && other.x == x && other.y == y

    }

    fun symmetryPoint(): Point {
        return  Point(x * -1, y * -1)
    }
}

val pointOne = Point(3, 8)
val PointTwo = Point(5, 13)

fun main() {

    println(pointOne.toString())

    println(pointOne == PointTwo)

    println(pointOne.symmetryPoint())
}